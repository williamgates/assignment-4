package bar;

import board.*;

import java.awt.*;
import javax.swing.*;

/**
 * This class handles the number of tries that the player has done in opening the cells.
 *
 * @author William Gates
 */
public class Status extends JPanel {
    private int count;
    private JLabel numberOfTries;
    private String text;

    /**
     * Constructor.
     */
    public Status() {
        count = 0;
        text = "Number of Tries: ";
        numberOfTries = new JLabel(text + count);

        add(numberOfTries);
    }

    /**
     * This method is used to increse the number of tries by one.
     */
    public void increment() {
        count += 1;
        numberOfTries.setText(text + count);
    }

    /**
     * This is the setter method for count.
     */
    public void setCount(int count) {
        this.count = count;
        numberOfTries.setText(text + count);
    }
}
